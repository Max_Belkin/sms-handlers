try:
    from urllib import urlopen, urlencode
except ImportError:
    from urllib.request import urlopen
    from urllib.parse import urlencode

from json import loads

from gate.models import SmsLog

TEST_MODE_ENABLED = True


def get_handler(handler_name):
    if handler_name == "SmscHandler":
        return SmscHandler()
    elif handler_name == "SmsTrafficHandler":
        return SmsTrafficHandler()
    raise NotImplementedError('This SMS handler have not been implemented')


class BaseSmsHandler():
    gate_url = ''
    test_gate_url = ''
    username = ''
    password = ''
    recipient = ''
    message = ''

    def set_common_parameters_if_given(self, user_data):
        self.username = user_data.get('username') or self.username
        self.password = user_data.get('password') or self.password
        self.recipient = user_data.get('recipient') or self.recipient
        self.message = user_data.get('message') or self.message

    def log_response(self, raw_answer):
        answer = loads(raw_answer)
        try:
            status = answer['status']
            phone = answer['phone']
        except KeyError:
            print("KeyError!!! Log message wasn't saved")
            return
        error_code = answer.get('error_code')
        error_msg = answer.get('error_msg')

        new_record = SmsLog(
            status=status,
            phone=phone,
            error_code=error_code,
            error_msg=error_msg,
        )
        new_record.save()
        return

    def send_and_save_response(self, user_data):
        self.set_common_parameters_if_given(user_data)

        parameters = urlencode({
            "http_username": self.username,
            "http_password": self.password,
            "phone": self.recipient,
            "message": self.message.encode(),
        })
        parameters = parameters.encode('utf-8')

        if self.recipient and self.message:
            if TEST_MODE_ENABLED:
                answer = urlopen(
                    self.test_gate_url,
                    data=parameters
                ).read().decode('utf-8')
            else:
                answer = urlopen(self.test_gate_url, data=parameters).read()

            self.log_response(answer)


class SmscHandler(BaseSmsHandler):
    username = 'sail_play_user'
    password = 'sail_play_password'
    gate_url = 'http://smsc.ru/some-api/message/'
    test_gate_url = 'http://127.0.0.1:8000/gate/smsc/'


class SmsTrafficHandler(BaseSmsHandler):
    username = 'sail_play_user_2'
    password = 'sail_play_password_2'
    gate_url = 'http://smstraffic.ru/super-api/message/'
    test_gate_url = 'http://127.0.0.1:8000/gate/smstraffic/'

