# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gate', '0008_auto_20150822_1742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='smslog',
            name='error_code',
            field=models.IntegerField(default=None),
        ),
        migrations.AlterField(
            model_name='smslog',
            name='error_msg',
            field=models.TextField(default=None),
        ),
    ]
