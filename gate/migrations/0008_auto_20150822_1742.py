# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gate', '0007_auto_20150822_1602'),
    ]

    operations = [
        migrations.AlterField(
            model_name='smslog',
            name='status',
            field=models.CharField(max_length=5),
        ),
    ]
