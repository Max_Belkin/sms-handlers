from django.db import models


class SmsLog(models.Model):
    status = models.CharField(max_length=5)
    phone = models.CharField(max_length=11)
    error_code = models.IntegerField(default=None, null=True)
    error_msg = models.TextField(default=None, null=True)
