from django.shortcuts import render
from django.http import HttpResponse

from json import dumps
from random import choice, random
from this import s


def center_gate(request):
    if request.method == "POST":
        phone = request.POST.get('phone')
        if random() > 0.5:

            wise_messages = s.splitlines()
            wise_messages = wise_messages[2:]
            answer = dumps(dict(
                status="error",
                phone=phone,
                error_code=(0.5-random())*1000,
                error_msg=choice(wise_messages)
            ))
        else:
            answer = dumps(dict(
                status="ok",
                phone=phone
            ))
        return HttpResponse(answer, content_type="application/json")
    return HttpResponse('empty here')
