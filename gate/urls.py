from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns(
    '',
    url(r'^smsc/$', 'gate.views.center_gate', name='center_gate'),
    url(r'^smstraffic/$', 'gate.views.center_gate', name='center_gate'),
)
