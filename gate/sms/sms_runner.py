import os
import sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "superlists.settings")
sys.path.append("../../")
# from gate.tools import SmscHandler, SmsTrafficHandler
from gate.tools import get_handler

if __name__ == '__main__':
    handler = get_handler('SmscHandler')
    handler.send_and_save_response(dict(recipient='79167371350', message="Good day!"))

    handler = get_handler('SmsTrafficHandler')
    handler.send_and_save_response(dict(recipient='79167371350', message="Nice day!"))
