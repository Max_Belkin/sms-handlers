from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns('',
    url(r'^$', 'gate.views.home_page', name='home'),
    url(r'^gate/', include('gate.urls')),

    # url(r'^admin/', include(admin.site.urls)),
)
